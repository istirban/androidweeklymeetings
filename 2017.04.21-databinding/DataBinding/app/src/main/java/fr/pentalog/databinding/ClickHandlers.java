package fr.pentalog.databinding;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Ionut Stirban on 29/03/2017.
 */

public class ClickHandlers {
    private Activity activity;

    public ClickHandlers(Activity activity) {
        this.activity = activity;
    }

    public void onDisplayFirstNameClicked(View view) {
        Toast.makeText(activity, "First name clicked", Toast.LENGTH_SHORT).show();
    }

    public void onDisplayLastNameClicked() {
        Toast.makeText(activity, "Last name clicked", Toast.LENGTH_SHORT).show();
    }
}
