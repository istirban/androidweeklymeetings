package fr.pentalog.databinding;

import android.databinding.BindingAdapter;
import android.text.Html;
import android.widget.TextView;

/**
 * Created by Ionut Stirban on 30/03/2017.
 */

public class BindingAdaptersList {
    private BindingAdaptersList(){}

    @BindingAdapter({"bind:fromHtml"})
    public static void setFromHtml(TextView view, String content) {
        view.setText(Html.fromHtml(content));
    }
}
