package fr.pentalog.databinding;

import android.databinding.ObservableField;

/**
 * Created by Ionut Stirban on 29/03/2017.
 */

public class Person {
    private ObservableField<String> firstName = new ObservableField<>();
    private ObservableField<String> lastName = new ObservableField<>();

    public Person(String firstName, String lastName) {
        this.firstName.set(firstName);
        this.lastName.set(lastName);
    }
    public ObservableField<String> getLastName() {
        return lastName;
    }

    public ObservableField<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }



}
