package fr.pentalog.databinding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import fr.pentalog.databinding.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Person person = new Person("Jake", "Gates");
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setPerson(person);

        //binding.txtFirstName.setText("Gabriel");

        binding.setHandlers(new ClickHandlers(this));

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                person.setFirstName("Jake 2");
            }
        }, 2000);

    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
