package fr.pentalog.resourcestest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showDetails(R.drawable.ic_data_usage_black_36dp);



    }

    private void showDetails(int image) {
        TextView txtDetails = (TextView) findViewById(R.id.txt_details);
        ImageView img = (ImageView) findViewById(R.id.iv_image);
        img.setImageResource(image);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), image);

        String details = "width: " + bitmap.getWidth() + " height: " + bitmap.getHeight() + ", 1000 dp = " + getResources().getDimensionPixelSize(R.dimen._1000_dp);
        txtDetails.setText(details);


//        String details = "1000 dp = " + getResources().getDimensionPixelSize(R.dimen._1000_dp);
//        txtDetails.setText(details);

    }
}
