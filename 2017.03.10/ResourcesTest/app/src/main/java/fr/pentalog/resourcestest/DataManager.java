package fr.pentalog.resourcestest;

import android.content.Context;

/**
 * Created by Ionut Stirban on 10/03/2017.
 */

public class DataManager {
    private static DataManager instance;
    private Context context;

    private DataManager(Context context) {
        this.context = context;
    }

    public static DataManager getInstance(Context context) {
        if (instance == null) {
            instance = new DataManager(context.getApplicationContext());
        }
        return instance;
    }

}
