package fr.pentalog.resourcestest;

import android.app.Application;

/**
 * Created by Ionut Stirban on 10/03/2017.
 */

public class ResApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
