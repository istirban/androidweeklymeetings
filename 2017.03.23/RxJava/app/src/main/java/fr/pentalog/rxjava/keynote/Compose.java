package fr.pentalog.rxjava.keynote;

import android.util.Log;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class Compose {
    public static <T> ObservableTransformer<T, T> addThreadStuff() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public void makeRequest() {
        getDataFromServer().
                compose(Compose.addThreadStuff())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {

                    }
                });

    }


    private Observable<String> getDataFromServer() {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(1000);
                Log.d("takeuntil", "returning server data");
                return "Server data";
            }
        });
    }
}
