package fr.pentalog.rxjava.keynote;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class WebService {
    public Observable<String> getFirstName() {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "Jake";
            }
        });
    }

    public static void test(){
        WebService ws = new WebService();

        Disposable disposable = ws.getFirstName()
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        int d = 0;
                        d++;
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        int d = 0;
                        d++;
                    }
                });
    }
}
