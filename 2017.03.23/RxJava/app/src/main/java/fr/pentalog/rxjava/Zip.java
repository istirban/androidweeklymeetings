package fr.pentalog.rxjava;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class Zip {
    private Observable<String> getApples() {
        return Observable.just("Apples");
    }

    private Observable<Double> getTomatoe() {
        return Observable.just(23d);
    }

    public void test() {
        Observable.zip(getApples(), getTomatoe(), new BiFunction<String, Double, String>() {
            @Override
            public String apply(String s, Double d) throws Exception {
                return s + d;
            }
        }).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {

            }
        });


    }
}
