package fr.pentalog.rxjava.keynote.tva;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public abstract class TextWatchAdapter implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
