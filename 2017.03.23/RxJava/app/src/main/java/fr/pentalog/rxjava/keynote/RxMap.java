package fr.pentalog.rxjava.keynote;

import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class RxMap {
    public static void test() {
        Observable.just(3)
                .map(new Function<Integer, Double>() {
                    @Override
                    public Double apply(Integer integer) throws Exception {
                        return integer + 0.33d;
                    }
                })
                .subscribe(new Consumer<Double>() {
                    @Override
                    public void accept(Double aDouble) throws Exception {
                        Log.d("double", "double" + aDouble);
                    }
                });
    }
}
