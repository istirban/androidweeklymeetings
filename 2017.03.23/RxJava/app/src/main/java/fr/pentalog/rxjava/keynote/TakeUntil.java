package fr.pentalog.rxjava.keynote;

import android.util.Log;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class TakeUntil {
    private static final int DESTROY = 1;
    private BehaviorSubject<Integer> behaviorSubject = BehaviorSubject.create();

    private Observable<String> getDataFromServer() {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(1000);
                Log.d("takeuntil","returning server data");
                return "Server data";
            }
        });
    }

    void getData() {
        getDataFromServer()
                .takeUntil(behaviorSubject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        Log.d("takeuntil","doFinally");
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        //this will not be called
                        Log.d("takeuntil","accept");
                    }
                });
    }

    private void onActivityDestroy() {
        behaviorSubject.onNext(DESTROY);
    }

    public static void test() {
        TakeUntil takeUntil = new TakeUntil();
        takeUntil.getData();
        takeUntil.onActivityDestroy();
    }
}
