package fr.pentalog.rxjava.keynote;

import io.reactivex.Observable;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class DoOnRetrolambda {
    public static void test() {
        Observable.just(3)
                .doOnSubscribe(disposable -> showLoading())
                .doFinally(DoOnRetrolambda::hideLoading)
                .subscribe(integer -> {
                    //do something with the integer
                }, throwable -> {
                    //handle the error
                });
    }

    private static void hideLoading() {

    }

    private static void showLoading() {

    }
}
