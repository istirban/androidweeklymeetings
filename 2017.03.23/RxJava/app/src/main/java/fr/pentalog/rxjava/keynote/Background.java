package fr.pentalog.rxjava.keynote;

import android.util.Log;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class Background {
    public static void test() {
        Observable
                .fromCallable(new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        Log.d("thread", "callable " + Thread.currentThread().getName());
                        return 5;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .map(new Function<Integer, Double>() {
                    @Override
                    public Double apply(Integer integer) throws Exception {
                        Log.d("thread", "map " + Thread.currentThread().getName());
                        return integer + 0.33d;
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Double>() {
                    @Override
                    public void accept(Double aDouble) throws Exception {
                        Log.d("thread", "accept " + Thread.currentThread().getName());
                        Log.d("double", "double" + aDouble);
                    }
                });
    }
}
