package fr.pentalog.rxjava.keynote.task;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ionut Stirban on 22/03/2017.
 */

public class TaskPresenter {
    private GetTask getTask;

    public TaskPresenter(GetTask getTask) {
        this.getTask = getTask;
    }

    public void doSomeWork(){
        this.getTask.executeUseCase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Task>>() {
                    @Override
                    public void accept(List<Task> tasks) throws Exception {
                        int d = 0;
                        d++;
                    }
                });
    }
}
