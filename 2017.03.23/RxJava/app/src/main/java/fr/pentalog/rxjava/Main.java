package fr.pentalog.rxjava;

/**
 * Created by Ionut Stirban on 24/03/2017.
 */

public class Main {
    public static void main(String[] args){
        int index;    //index
        int n;        //maximum number

        n = 30;
        for(index = 1 ; index <= n ; index++)
        {
            if(index % 5 == 0 && index % 6 == 0)
            {
                System.out.print("XY ");
                index++;
            }
            else
            {
                if(index % 5 == 0)
                {
                    System.out.print("Z ");
                    index++;
                }
                if(index % 6 == 0)
                {
                    System.out.print("Y ");
                    index++;
                }
            }

            if(index <= n)
                System.out.print(index + " ");

        }
    }
}
