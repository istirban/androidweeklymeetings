package fr.pentalog.rxjava;

import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ionut Stirban on 23/03/2017.
 */

public class SwitchIfEmpty {
    private Observable<String> getDataLocal() {
        return Observable.empty();
    }

    private Observable<String> getDataFromServer() {
        return Observable.just("Server value");
    }

    public Observable<String> getData() {
        return getDataLocal()
                .switchIfEmpty(getDataFromServer());
    }

    public static void test() {
        SwitchIfEmpty switchIfEmpty = new SwitchIfEmpty();
        switchIfEmpty.getData()
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        Log.d("value", "value " + s);
                    }
                });
    }
}
