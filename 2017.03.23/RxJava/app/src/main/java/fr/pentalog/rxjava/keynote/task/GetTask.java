package fr.pentalog.rxjava.keynote.task;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;

/**
 * Created by Ionut Stirban on 22/03/2017.
 */

public class GetTask {
    public Observable<List<Task>> executeUseCase() {
        return Observable.fromCallable(new Callable<List<Task>>() {
            @Override
            public List<Task> call() throws Exception {
                return Collections.singletonList(new Task());
            }
        });
    }
}
