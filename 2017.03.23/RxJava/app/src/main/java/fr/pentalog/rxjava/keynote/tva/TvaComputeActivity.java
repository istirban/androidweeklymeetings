package fr.pentalog.rxjava.keynote.tva;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.widget.EditText;

import fr.pentalog.rxjava.R;
import fr.pentalog.rxjava.keynote.TakeUntil;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class TvaComputeActivity extends AppCompatActivity {
    private Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tva_compute);
        //buildObservables();
        TakeUntil.test();


    }

    private void buildObservables() {
        EditText edtTotalPrice = (EditText) findViewById(R.id.edt_total_price);
        EditText edtTva = (EditText) findViewById(R.id.edt_tva);

        disposable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
                edtTotalPrice.addTextChangedListener(new TextWatchAdapter() {

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(!emitter.isDisposed()) {
                            emitter.onNext(s.toString());
                        }
                    }
                });
            }
        }).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                if (s.isEmpty()) {
                    edtTva.setText("");
                    return;
                }
                edtTva.setText(String.valueOf(Integer.valueOf(s) * 0.25d));
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
